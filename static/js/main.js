var game = new Object;
game.player1 = new Object;
game.player1.name = "Player 1";
game.player1.number = 1;
game.player1.color = "red";
game.player1.mark = "x";
game.player1.wins = 0;


game.player2 = new Object;
game.player2.name = "Player 2";
game.player2.number = 2;
game.player2.color = "cyan";
game.player2.mark = "o";
game.currentPlayer = game.player1;
game.player2.wins = 0;

const myStorage = window.localStorage;

document.onload = loadStats(allStorage(myStorage));

function submitHandler(){
    console.log('Submit button pressed')
    loadGameObject();
    checkRate(game.player1);
    checkRate(game.player2);
    loadStats(allStorage(myStorage));
    createBoard();
}

function resetHandler(){
    console.log('Reset button pressed');
    const element = document.getElementById("message");
    element.innerHTML = "";
    element.removeAttribute('style')
    submitHandler();

}
function createBoard() {
    let field = document.getElementById('gameField');
    field.innerHTML = '';
    field.setAttribute('style', `grid-template-columns: repeat(${game.columns}, auto)`);

    document.getElementById("message").innerHTML = `${game.currentPlayer.name},  make your move.`;

    // create 2D array
    let arr = new Array(game.rows);
    game.board = arr;

    for (var i = 0; i < arr.length; i++) {
        arr[i] = new Array(game.columns);
    }

    //init 2D array
    for (i = 0; i < game.columns; i++) {

        for (var j = 0; j < game.rows; j++) {
            let cell = document.createElement('div');
            cell.setAttribute('id', `${i}_${j}`);
            cell.setAttribute('class', 'cell');
            cell.addEventListener('click', function (e) {
                const coords = this.id.split("_");
                const row = coords[0];
                const column = coords[1];
                this.setAttribute('class', `cell ${game.currentPlayer.mark}`);
                game.board[row][column] = game.currentPlayer.number;
                checkEnd(row, column) ? finish() : changePlayer();
            }, {
                once: true,
                capture: false
            });
            field.appendChild(cell);
            arr[j][i] = 0;

        }

    }
}

function loadGameObject() {
    game.columns = parseInt(document.getElementById("board-columns").value);
    game.rows = parseInt(document.getElementById("board-rows").value);
    let steps = parseInt(document.getElementById("board-steps").value);
    if (steps > game.rows && steps > game.columns) {
        steps = Math.max(game.rows, game.columns);
        document.getElementById("board-steps").value = steps;
    }
    game.steps = steps;
    game.player1.name = document.getElementById("player1name").value ? document.getElementById("player1name").value : "Player 1";
    game.player2.name = document.getElementById("player2name").value ? document.getElementById("player2name").value : "Player 2";
}

function checkRate(player){
    let rate = myStorage.getItem(`${player.name}`) ;
    if (rate === null) {
        myStorage.setItem(`${player.name}`, 0);
        player.wins = 0;
    } else player.wins = rate;
}



function checkEnd(row, column ){
    return (checkRows(row, column) || checkColumns(row, column) || checkDiagonals(row, column));
}

function checkRows(row, column){
    // console.log("Checking rows");
    let counter = 0;
    let steps = game.steps;
    let startColumn = (column - game.steps) > 0 ? (column-game.steps) : 0;
    let endColumn = (parseInt(column) + parseInt(steps)) < parseInt(game.columns) ? (parseInt(column) + parseInt(steps) - 1) :  (game.columns - 1);

    for (let i = startColumn; i <= endColumn; i++) {
        // console.log(`startColumn=${startColumn} endColumn=${endColumn} i=${i} row=${row} column=${column} cell=${game.board[row][i]}`)
        if (game.board[row][i] === game.currentPlayer.number) {
            counter++;
        } else {
            counter = 0;
        }
        if (counter == game.steps) {
            return true;
        }
    }
}

function checkColumns(row, column){
    // console.log("Checking columns");
    let counter = 0;
    let steps = game.steps;
    let startRow = (row - steps) > 0 ? (row-steps) : 0;
    let endRow = (parseInt(row) + parseInt(steps)) < parseInt(game.rows) ? (parseInt(row) + parseInt(steps) - 1 ) : ( game.rows - 1) ;

    for (let i = startRow; i <= endRow; i++) {
        // console.log(`startRow=${startRow} endRow=${endRow} i=${i} row=${row} column=${column} cell=${game.board[i][column]}`)
        if (game.board[i][column] === game.currentPlayer.number) {
            counter++;
        } else {
            counter = 0;
        }
        if (counter == game.steps) {
            return true;
        }
    }
}

function checkDiagonals(row, column){
    // console.log("Checking 1 diagonal");
    let steps = game.steps;
    let startRow = row-steps+1;
    let startColumn = column-steps+1;
    let counter=0;

    if (startRow<0 || startColumn<0) {
        let min = Math.min(row,column);
        startRow = row-min;
        startColumn = column-min;
    }
    for (let i = 0; ( (startRow+i)<game.rows && (startColumn+i)<game.columns ); i++) {
        // console.log(`startRow=${startRow} startColumn=${startColumn} i=${i} cell=${game.board[startRow+i][startColumn+i]}`)
        if (game.board[startRow+i][startColumn+i] === game.currentPlayer.number) {
            counter++;
        } else {
            counter = 0;
        }
        if (counter == game.steps) {
            return true;
        }
    }

    // console.log("Checking 2 diagonal");
    startRow = parseInt(row)+steps-1;
    startColumn = column-steps+1;
    // console.log(`Before: startRow=${startRow} startColumn=${startColumn}`)
        if (startRow>=game.rows || startColumn<0) {
        let max = Math.max(startRow-game.rows+1, 0-startColumn);
        startRow = startRow-max;
        startColumn = startColumn+max;
    }
    // console.log(`After: startRow=${startRow} startColumn=${startColumn}`)
    counter=0;
    for (let i = 0; ( (startRow-i)>=0 && (startColumn+i)<game.columns ); i++) {
        let currX = startRow - i;
        let currY = startColumn + i;
        let currValue = game.board[currX][currY];
        // console.log(`currX=${currX}  currY=${currY}  currValue=${currValue} startRow=${startRow} startColumn=${startColumn} i=${i}` )
        if ( currValue === game.currentPlayer.number) {
            counter++;
        } else {
            counter = 0;
        }
        if (counter == game.steps) {
            return true;
        }
    }
}

function finish(){
    const element = document.getElementById("message");
    element.innerHTML = `${game.currentPlayer.name} won !!!!`;
    element.setAttribute('style', 'color: red')
    document.getElementById("gameField").outerHTML=document.getElementById("gameField").outerHTML;  
    myStorage.setItem(`${game.currentPlayer.name}`, parseInt(game.currentPlayer.wins)+1);
}

function changePlayer(){
    if (game.currentPlayer == game.player1) {
        game.currentPlayer = game.player2;
    } else {
        game.currentPlayer = game.player1;
    }
    document.getElementById("message").innerHTML = `${game.currentPlayer.name},  make your move.`

}

function allStorage(mystorage) {
    var stats = new Map();
    var keys = Object.keys(localStorage);
    keys.forEach(v => {
        stats.set(v, mystorage.getItem(v))
    });
    return stats;
}

function loadStats(stats){
    const field = document.getElementById("stats");
    field.innerHTML='<div>Local wins rate:</div>';
    stats.forEach((v,k)=>{
        let element = document.createElement('div')
        element.innerHTML=`${k} : ${v} wins`
        field.appendChild(element);
    })
}